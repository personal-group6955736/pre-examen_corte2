import express from 'express';
import { fileURLToPath } from 'url';
import path from 'path';

import myRoutes from './router/index.js';

const puerto = 80; // Puerto 3000 alt
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Crear la aplicación Express
const main = express();

// Configuración del motor de vistas EJS
main.set('view engine', 'ejs');

// Configuración del middleware para servir archivos estáticos desde el directorio 'public'
main.use(express.static(path.join(__dirname, 'public')));

// Configuración del middleware para analizar datos de formulario
main.use(express.urlencoded({ extended: true }));

// Configuración de las rutas
main.use(myRoutes.router);

main.listen(puerto, () => {
  console.log(`Iniciando servidor en http://localhost:${puerto}`);
});